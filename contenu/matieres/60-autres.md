# Autres languages du web:

PHP, Node.js, Ruby, Django, Python, Perl

Markdown

## PHP: 

- Frameworks PHP
- Symphony, Lavarel
- CMS en PHP: WordPress, Drupal, Joomla

- Agences spécalisées WordPress...
- en France: BeAPI, Whodunit.fr
- en Suisse: required.ch - a utilisé WordPress pour des clients comme les CFF.
- International: Human Made, hmn.md

- Agences spécialisées Symphony + Drupal
- Liip AG - Zurich, Lausanne, Fribourg. A utilisé Drupal pour des clients comme MIGROS.

- Agences spécialisées Typo3:
- Ttree.ch - Lausanne
- A utilisé Typo3 pour des clients comme Visions du Réel, le Festival du film de Locarno...


### Notions de base PHP: 

- Syntaxe <?php ?>
- Fonctions: Function()
- Variables
- Time()
- Include
- Array()

## MySQL

Connaissance utile: exporter une base de données MySQL

- connexion a PHPMyAdmin: une interface web pour l'administration de MySQL.
- réglages d'export

Migrer une base de données MySQL. Une base de données pourra atteindre un poids assez conséquent ... qui peut dépasser la limite d'upload de l'interface web. Dans ce cas, il existe des scripts pratiques d'importation.

# CMS

- choisir un CMS
- (WordPress, Drupal, Joomla, Typo3...)
- Evolution et vitalité des CMS

Static site generators... Jeckyll etc. Une alternative aux CMS, pour les gens qui préfèrent les fichiers statiques et le minimalisme, et ont un besoin réduit en fonctionalité.

# Performance: rapidité frontend et backend

- mesurer la vitesse de chargement
- outils de mesure
- services web (pingdom)

# Emails et newsletters

- Principe de l'email, serveurs d'envoi
- Autres technologies: mailbox
- Principe de la mailing-liste, liste de diffusion.

Services facilitant l'envoi de newsletters: MailChimp, ... 


