# Préambule, notions de base:

- internet, web
- site internet
- http
- ftp
- noms de domaine
- hébergement

Acheter un nom de domaine

Mise en place du système DNS. Première vente de nom de domaine.

Les noms de domaine initiaux: .com, .org, .net, .mil...

Les noms de domaine par pays: .ch, .li, .fr, .it, .de.... quelques cas spécaux: .tv, .fm, .co, .io

Ajout au compte-goutte de nouvelles extensions (.biz, .coop, .museum).

Révolution, un grand nombre de nouveaux noms de domaine est introduit. .swiss ...

## Choisir un hébergement web

- international

Tarifs d'hébergement.

Types d'hébergement: Shared Hosting; VPS; 

Services offerts par un hébergeur:

- Hébergement de données (limite?)
- Bande passante (limite?)
- Accès FTP (et SFTP, SSH?)
- Garantie d'uptime (99,9% ?)
- Administration de noms de domaines, gestion DNS
- Emplacement des datacenters (adresses IP, prises en compte par Google)
- Certificat SSL
- Qualité de l'interface d'administration
- Langages proposés (version de PHP, MySQL)... autres langages?)
- Hébergement email inclus?

Hébergements spécialisés pour CMS

Hébergements optimisés WordPress:
WPEngine. 

Hébergement pour NodeJS, Ruby, Django...

Heroku,
Linode,
DigitalOcean,
Amazon

### Mesurer la disponibilité d'un site

Un outil de mesure: pingdom

## Hébergement email

Protocoles email: POP, IMAP (autres protocoles: Exchange).

Souvent inclus dans un "pack" d'hébergement

Hébergeurs spécifiques pour les email:

Google,
Zoho

Clients email:

Apple Mail,
Thunderbird,
Postbox,
Airmail, Mailpilot...

“Never Check Email In the Morning“

# Outils et méthodes

- Conception d'un projet web: étude préalable, brainstorming, phase de recherche, croquis, prototypage.

- Maquettes graphiques, quels outils? Sketch, Invision, Balsamiq

## Méthodologies

![Phases d'un projet web, par Thierry Pigot, WordCamp Paris](../images/phases-de-projet.png)

Phases d'un projet web selon les standards d'une SSII:

- Conseil - qualifier le besoin du client
- Etude - analyser, formaliser le besoin  
- Spécifications - écrire en détail toutes les fonctions
- Ergonomie - accessibilité
- Direction artistique - conception des maquettes
- Création graphique - mise en oeuvre des préconisations du directeur artistique
- Intégration HTML / CSS / JS - 
- Installation et paramétrage de WP
- Templating - transformation en thème WordPress
- Développement spécifiques - création de plugins, fonctions
- Intégration du contenu - 
- Tests
- Hébergement
- Mise en pré-production - 
- Test de charge
- Recette: le client valide le fonctionnement
- Mise en production
- Site en ligne
- Debug / correction: maintenance corrective
- Maintenance évolutive

Méthodes agiles, fin de la méthodologie "en cascade".

L'approche Design Studio

Méthodes pour démarrer un projet (Kickoff Meeting):

### "20 Second Gut Test Exercise"
- montrer aux participants une sélection de sites pouvant servir d'inspiration (env. 20) 
- chaque participant les évalue sur une échelle de 1 à 5.
- les 5 meilleurs et 5 pires sites sont discutés.

Source: 

- http://goodkickoffmeetings.com/2010/04/the-20-second-gut-test/
- http://bradfrost.com/blog/post/establishing-design-direction/

### Design Studio/Prototyping Exercise

http://goodkickoffmeetings.com/2010/04/design-studioprototyping-exercise/

Article de Brad Frost: 
"The Post-PSD Era" (2013)

Article de Emelyn Baker:
"2014 - The Year of Interaction Design Tools"

Agile UX: mise en place de "sprints", phases de travail qui réunissent designers, dévelopers et chefs de projet.

Outils de prototypage (en ligne), apparus entre 2011 - 2013: 

- UXPin (paper, mobile, web, responsive)
- InVision (mobile, web)
- Flinto (mobile)
- POP – Prototyping on Paper (mobile)
- Marvel (mobile, web)...

À lire: article de Emily Schwartzman qui compare un grand nombre d'outils:
http://www.cooper.com/prototyping-tools

## Applications de gestion de projet

- Basecamp
- Trello
- GitHub

##  Développement et code

Un outil de développement: Atom

Configurer Atom pour un projet web: 
- configurer la synchro FTP

### Configurer Atom pour un projet web

Quelques astuces pour la bonne utilisat d'Atom.

Note: pour bien fonctionner, Atom exige d'être placer dans le dossier Applications de MaxOSX. Vous risquez de voir des erreurs si vous le lancez depuis un autre emplacement.

Aller dans Atom > Préférences: 
- Sous *Keybindings*, vous verrez la liste des raccourcis clavier.
- Sous *Packages*, vous verrez la liste des Packages (extensions) installés.
- Sous *Themes*, vous pouvez définir le thème de l'interface. Vous pouvez choisir un thème UI pour l’interface utilisateur, et un thème de Syntaxe.

- ouvrir un projet: il est possible d'ouvrir un projet: avec la fonction File > Open, on peut choisir un dossier de projet (veiller à ne pas séléctionner de fichier). Cela permet de voir l’arborescence, utile quand on travaille sur un projet web.

Ajouter des fonctionnalités supplémentaires avec des Packages

- autoclose-html : pour fermer automatiquement les balises HTML.
- webbox-color : pour prévisualiser les couleurs CSS.
- color-picker : pour avoir un sélecteur de couleur.
- linter (et linter-csslint, linter-htmlhint) : pour détecter les erreurs de syntaxe dans votre code.
- remote-sync : pour permettre la syncronisation automatique avec un serveur FTP.
- livereload : pour recharger la fenêtre de navigateur dès que vous sauvez votre projet.
- minimap : pour avoir la vue miniature du code, commme dans Sublime Text.

Autres outils de développement : Sublime Text, Brackets, Espresso (particulièrement utile pour la prévisualisation du CSS), Coda. Dreamweaver. PHPStorm.

Utilitaires de développement : Grunt (task-runner), Composer...

## Outils de communication et collaboration

Comment communiquer au sein d'un projet?
Communication en temps réel: 
Slack, Hipchat...
Basecamp

Outils de *collaboration au sein d'une équipe*: Yammer, Asana, Trello, Basecamp, Notes, etc.

Outils pour le *partage de fichiers et de documents*: Box, Google Drive, Dropbox, etc.

Outils de *communication en équipe*: Slack, HipChat etc.

## Utilitaires divers

Outils de time-tracking
Comptabilité et gestion de projet

Méthodologies de travail - Pomodoro Method...
