# Glossaire des termes

API

ARIA : Accessible Rich Internet Applications

**Backend :** le code "coté serveur", par opposition au Frontend (voir ci-dessous).

**Basecamp :** un service web facilitant la gestion de projet. Utilisé par UX Romandie ainsi que WordCamp Switzerland, pour l'organisation de conférence. Au-delà de cet outil, les créateurs de Basecamp ont contribué au langage Ruby on Rails, et ont publicé les livres "Re:Work" et ... , guides à la création de startup basée sur un modèle économique "agile".

CoffeeScript

CSS

CSS Variables

CSS Flexbox

CSS Regions

CSV

**DNS :** Domain Name System: le système des noms de domaine qui régit l'architecture de l'internet.

DOM

epub

**Frontend :** le code "coté client", càd le code HTML, CSS et JavaScript qui est traité par un navigateur. Désigne le travail de l'intégrateur, par opposition au "backend" programmé en PHP (ou autre langage traité côté serveur, p.ex. Ruby).

**FTP :** File Transfer Protocol, méthode utilisée pour le transfer de fichier depuis un ordinateur vers un serveur distant. À noter qu'on préfère sa version sécurisée, SFTP.

Github

Gravatar

HTML

JavaScript

Jekyll

JSON

Liquid

Markdown

**mobi :** format de livre électronique.

**PDF :** format de document électronique.

**Recette (informatique) :** En informatique, la recette (ou test d'acceptation) est une phase de développement des projets, visant à assurer formellement que le produit est conforme aux spécifications. Elle s'inscrit dans les activités plus générales de qualification. En anglais on dit: *acceptance testing*. Le *cahier de recette* est la liste exhaustive de tous les tests pratiqués par le fournisseur avant la livraison du produit. 

Sass

SEO : Document Object Model

**SFTP :** Secure FTP, une version sécurisée du FTP. À préconiser pour garantir la sécurité de vos mots de passe, en particulier si vous travaillez sur un Wifi public.

SMACSS

**SSH :** Un protocole permettant de se connecter à un serveur distant via la ligne de commande (terminal). La commande à effectuer est généralement la suivante: 
ssh username@server.example.com
Cela donne accès à des opérations qui ne sont pas possibles via FTP, comme dézipper un fichier. Parmi les opérations fréquentes: Changements précis des droits d'accès sur les fichiers et dossiers. Opérations d'effacement ou déplacement des fichiers extrêmement rapides.
 
**SSII :** société de services en ingénierie informatique (SSII ou SS2I).
 
**SSL (certificat SSL) :** permet une connexion sécurisée à un site Internet. À utiliser pour des services où des utilisateurs transmettent des données personnelles, comme des mots de passe voire des informations bancaires : une connexion sans SSL peut être facilement interceptée par une tierce personne.

**TJM :** taux journalier moyen. Le taux moyen donnée par web-costing.com : TJM Freelance = 390€, Agence Web = 410€, SSII = 460€, Structure offshore = 200€.

WCAG = Web Content Accessibility Guidelines

YAML