# HTML, CSS, JS

## HTML:

- Syntaxe

- Balises fondamentales

- Questions de sémantique

- HTML5 

Exemples de questions qu'on se pose en HTML: 

### Comment mettre en code une navigation? 

https://css-tricks.com/navigation-in-lists-to-be-or-not-to-be/

https://css-tricks.com/wrapup-of-navigation-in-lists/


Accessibilité:

http://alistapart.com/blog/post/on-alt-text

## Accessibilité : écrire un HTML accessible

## CSS:

- Syntaxe

### Principes fondamentaux du CSS:

- cascading
- specificity rules
- selectors
- inheritance
- box model
- stacking context

"Fundamental concepts of CSS like cascading, specificity rules, selectors, inheritance, box model and stacking context must be well understood."

- Comment inclure le CSS dans une page
- Bonnes pratiques

Préprocesseurs ! Less, Sass ...

## Mise en page en CSS

CSS Grid

CSS Grid Layout

Three years with CSS Grid Layout
03 November 2015
Rachel Andrew

### Références et infos

*Le tournant des CSS vers le Layout*    
Conférence de Daniel Glazman, à Paris Web 2012    
http://www.paris-web.fr/2012/conferences/le-tournant-des-css-vers-le-layout.php
Présente diverses nouveautés: CSS Variables, CSS Flexbox, CSS Regions, CSS Exclusions and Shapes, CSS Grids, Blending & Compositing, Web Fonts, CSS Filter Effects.

*An Introduction to CSS Grid Layout*    
Conférence de Rachel Andrew, à Paris Web 2015    
http://www.paris-web.fr/2015/conferences/an-introduction-to-css-grid-layout.php
Présente le module CSS Grid Layout.

http://www.frontendhandbook.com/learning/html-css.html

http://fr.learnlayout.com/

## Webfonts

Méthode classique: le "fontstack". 

Exemple, pour déclarer une fonte: 
```css
body {
   font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; 
   font-weight: 300;
}
```


## JavaScript (JS)

### Syntaxe

### Principes d'utilisation

### La librairie jQuery

JavaScript n'a rien à voir avec Java, un autre language de programmation. Il a été créé en 1995, pour Netscape Navigator, par un certain Brendan Eich. Le langage a été créé en 10 jours, pour coïncider avec la sortie de Netscape 2.0. Microsoft, pour son navigateur Internet Explorer, a fait du "reverse-engineering", pour introduire en 1996 JScript, dans IE version 3. En fin 1996, Netscape entreprend des démarches pour faire de JavaScript un standard (qui s'appelle ECMAScript). Ce standard est révisé régulièrement, la 6ème version (ES6) a été finalisée en juin 2015.

De nombreux utilitaires ont été construits sur la base du JavaScript:

- **jQuery** est une *bibliothèque* JavaScript (library) - une boîte à outils pour faciliter les usages les plus communs.
- **Node.js** est une *plateforme serveur* JavaScript (runtime) - comme Apache, mais exécutant du code JavaScript plutôt que du PHP.
- **AngularJS** est un *framework* de programmation JavaScript front-end, créé par Google.
- **Ember** est un autre *framework* front-end JavaScript.
- **React** est encore un *framework* JavaScript front-end, créé par l'équipe de développement de Facebook.

Une application web contemporaine, au lieu d'utiliser LAMP (Linux, Apache, MySQL, PHP), pourrait tourner sur MEAN, acronyme pour l'utilisation de MongoDB (base de données), Express.js, Angular.js, et Node.js - une base logicielle entièrement codée en langage JavaScript.

Préprocesseurs... **CoffeeScript** est un langage de programmation plus épuré que le JavaScript, pouvant ensuite être automatiquement converti en JavaScript normal.

Apprendre le JavaScript: une approche proposée par Remkus de Vries:

https://remkusdevries.com/learning-javascript-in-wordpress-deeply/

Deux livres recommandés:

https://tommcfarlin.com/recommended-javascript-books/

### ES6

Qu'est-ce que ES6 ? Une nouvelle version du standard JavaScript, dont la spécification a été finalisée en 2015.

Table de compatibilité JavaScript:
http://kangax.github.io/compat-table/es6/ 

## CSS layout - créer des mises en page en CSS

Méthodes de mise en page CSS: 

- In the mean time, I'd use floats for column-based layouts rather than flexbox.

- floats? table-cell ftw!

- Why doesn't anyone use or suggest inline-block? I'm always shocked at the prominent use of floats for layout. It has the best support, needs no hacks or clear-fixes
- Inline-block is great and works in old-ie with zoom:1. You can do some cool stuff with it + text-align:justify too.

"column floats are a hack and will be replaced by grid. You'd only want to use column floats for legacy reasons."
"flexbox isn't applicable for flexible grids. Flexbox can distribute boxes along a row or column, but not both at the same time, which is what you need for a grid. CSS grid does this."

https://jakearchibald.com/2014/dont-use-flexbox-for-page-layout/

"Flexbox and grid play well together, and are a huge step forward from the float & table hacks they replace. The sooner we can use them both in production, the better."

http://caniuse.com/#feat=css-grid

http://caniuse.com/#search=flexbox

voir aussi http://html5please.com/

## Le design Responsive

http://ricg.io/

responsive issues community group.
The RICG is a group of independent designers and developers working toward new web standards that will build fast, accessible, responsive websites.


http://usecases.responsiveimages.org/

https://www.w3.org/community/respimg/

https://24ways.org/2012/redesigning-the-media-query/

http://csslayout.news/

http://www.paris-web.fr/2015/conferences/an-introduction-to-css-grid-layout.php

## Frameworks et utilitaires

### Bootstrap

Une librairie modulaire pour développement frontend. Créé par Mark Otto et Jacob Thornton, alors employés de Twitter, en 2011. Bootstrap est open source et partagé sur Github - il s'agit d'ailleurs du projet le plus populaire sur github, avec plus de 90'000 "stars" et plus de 38'000 "forks". Plus de 600 personnes ont contribué au code source.

La version 4.0 alpha (en août 2015) apporte la transition à de Less à Sass, et l'usage de Flexbox pour la mise en page: http://blog.getbootstrap.com/2015/08/19/bootstrap-4-alpha/

Bootstrap is modular and consists essentially of a series of LESS stylesheets that implement the various components of the toolkit. A stylesheet called bootstrap.less includes the components stylesheets. Developers can adapt the Bootstrap file itself, selecting the components they wish to use in their project.

http://getbootstrap.com/

## Jekyll

Un générateur de site statique, écrit en Ruby. Créé par Tom Preston-Werner (1979), développeur, fondateur de Github, créateur de Gravatar. Jekyll est un logiciel open-source, sous licence MIT.