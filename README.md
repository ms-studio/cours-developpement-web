# Développement web

## Support de cours

Cet ouvrage est un support de cours évolutif, conçu pour le cours Développement Web, proposé au programme d'études de l'Eracom.

## Comment générer le livre?

Naviguer jusqu'au dossier du livre avec le Terminal.

Effectuer la commande :

bash scripts/build.sh

Un fichier PDF sera généré dans le dossier "sortie".

***